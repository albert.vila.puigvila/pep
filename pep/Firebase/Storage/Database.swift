//
//  Database.swift
//  pep
//
//  Created by albert vila  on 27/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import FirebaseFirestore


class Database: FSCollectionReference, FSStorageProtocol {
    
    static let shared = Database()
    
    private var listener:ListenerRegistration?
    
    // MARK: - Public -
    func stopListener() {
        guard let observer = listener else {
            return
        }
        observer.remove()
        listener = nil
    }
    
    func getPictures(season: SeasonType,
                     source: FirestoreSource = .server,
                     handler: @escaping ResultResponse<[Picture]>)
    {
        listener = season.toReference.addSnapshotListener { snap, error in
            guard let query = snap else {
                assertionFailure();
                handler(.failure(PepError.fileNotExists)); return
            }
            
            let pictures = query.documents.map { $0.data() }.toPicture()
            pictures.getURLs(season: season, handler: { photosURLs in
                guard !photosURLs.isEmpty else {
                    handler(.failure(PepError.empty)); return
                }
                handler(.success(photosURLs.sorted(by: { $0.createdAt > $1.createdAt })))
            })
        }
    }
    
    func getPhotosName(name: String, season: SeasonType, handler:@escaping ResultResponse<String>) {
        let path = PEPBuckets.buildExistingPath(season, name)
        let ref = storage.reference().child(path)
        
        ref.downloadURL { url, error in
            guard error == nil else {
                handler(.failure(error!)); return
            }
            guard let urlString = url?.absoluteString else {
                handler(.failure(PepError.fileNotExists)); return
            }
            handler(.success(urlString))
        }
    }
    
    func sendToStorage(image: UIImage, season: SeasonType, handler:@escaping ResultResponse<Error?>) {
        guard let quality = calculateQuality(of: image) else {
            handler(.failure(PepError.imageSoBig))
            return
        }
        
        guard let data = image.jpegData(compressionQuality: quality) else {
            assertionFailure()
            handler(.failure(PepError.fileNotExists)); return
        }
        let name = PEPBuckets.buildNewPath(season)
        let ref  = storage.reference().child(name)
        let metadata = StorageMetadata()
        
        /// 1. put in storage
        _ = ref.putData(data, metadata: metadata) { metadata, error in
            guard error == nil else {
                assertionFailure()
                handler(.failure(error!)); return
            }
            guard let newMetadata = metadata, let fileName = newMetadata.name else {
                handler(.failure(PepError.fileNotExists)); return
            }
            let params = self.makeParams(url: fileName, createdAt: Date().timeIntervalSince1970)
            
            /// 2. add ref in database
            season.toReference.document(fileName).setData(params, completion: { error in
                guard error == nil else {
                    assertionFailure();
                    self.deletePhotoInStorage(fileName: fileName, season: season, handler: { error in
                        handler(.failure(PepError.imageNoUploaded))
                    })
                    handler(.failure(PepError.imageNoUploaded)); return
                }
                handler(.success(nil))
            })
        }
    }
    
    func deletePhotoInStorage(fileName: String, season: SeasonType, handler:@escaping (Error?) -> Void) {
        let name = PEPBuckets.buildExistingPath(season, fileName)
        let ref  = storage.reference().child(name)
        ref.delete { error in
            handler(error)
        }
    }
    
    func deletePhotoFromEverywhere(name: String, season: SeasonType, handler:@escaping (Error?) -> Void) {
        deletePhotoInStorage(fileName: name, season: season) { error in
            guard error == nil else {
                assertionFailure();
                handler(error!); return
            }
            season.toReference.document(name).delete(completion: { error in
                guard error == nil else {
                    assertionFailure()
                    handler(error!); return
                }
                handler(nil)
            })
        }
    }
    
    private func makeParams(url: String, createdAt: TimeInterval) -> [String: Any] {
        return ["name" : url, "createdAt" : String(createdAt)]
    }
    
    private func calculateQuality(of image: UIImage) -> CGFloat? {
        guard let data = image.pngData() else {
            return nil
        }
        let bytes:Int = data.count
        switch bytes {
        case 5242881...10485760:
            return 0.75
        case 10485761...20971520:
            return 0.5
        case 20971521...104857600:
            return nil
        default:
            return 0.8
        }
    }
}

// MARK: - Helper to set new doc manually in Database -
extension Database {
    
    func helperToSetDocumentInDatabase(collection: CollectionReference, name: String, params: [String: Any]) {
        collection.document(name).setData(params) { error in
            guard let error = error else {
                print("😂 Success"); return
            }
            print("😤 \(error.localizedDescription)")
        }
    }
    func setDocuments(season: SeasonType) {
        ListPhotos().list.helperToSend(collection: season.toReference)
    }
}

// MARK: - Helper -
private extension Array where Element == [String: Any] {
    
    func toPicture() -> [Picture] {
        var pictures = [Picture]()
        
        forEach { item in
            var picture = Picture()
            
            if let createdAt = item["createdAt"] as? String {
                picture.createdAt = createdAt
            } else {
                assertionFailure();
            }
            
            if let name = item["name"] as? String {
                picture.name = name
            } else {
                assertionFailure();
            }
            pictures.append(picture)
        }
        return pictures
    }
    
    func helperToSend(collection: CollectionReference) {
        let database = Database()
        
        forEach { params in
            if let name = params["name"] as? String {
                database.helperToSetDocumentInDatabase(collection: collection, name: name, params: params)
            } else {
                print("🤯 nooooorrrrrrrrr...")
            }
        }
    }
}

struct ListPhotos {
    var list:[ [String: Any] ] {
        return [
            ["createdAt" : "1546167324.0", "name" : "37ABE6E7-D47B-4A68-8591-A7B89FD9CBEE.jpg"],
            ["createdAt" : "1546167324.1", "name" : "6118A274-4EDE-4CC4-B7C1-4BD48A6D76BA.jpg"],
            ["createdAt" : "1546167324.2", "name" : "6BD7BBD1-9C26-4879-A13B-0E6D28EBC5BB.jpg"]
        ]
    }
}

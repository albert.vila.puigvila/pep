//
//  FirestoreDatabase+Reference.swift
//  pep
//
//  Created by albert vila  on 27/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//


import FirebaseDatabase
import FirebaseFirestore

protocol FirebaseDataBasable {
    var dataBase:Firestore { get }
}

class FirebaseManager:FirebaseDataBasable {
    
    static let shared = FirebaseManager()
    
    var dataBase: Firestore {
        return Firestore.firestore()
    }
    
    // MARK: - Life Cycle -
    private init() {
        let settings = dataBase.settings
        settings.areTimestampsInSnapshotsEnabled = true
        dataBase.settings = settings
    }
}


protocol FSCollectionReference { }

fileprivate extension FSCollectionReference {
    var db:Firestore {
        return FirebaseManager.shared.dataBase
    }
}

// MARK: - Var's -
extension FSCollectionReference {
    
    var child: CollectionReference {
        return db.collection("child")
    }
    var university: CollectionReference {
        return db.collection("university")
    }
    var young: CollectionReference {
        return db.collection("young")
    }
    var nieces: CollectionReference {
        return db.collection("nieces")
    }
    var old: CollectionReference {
        return db.collection("old")
    }
    var travels: CollectionReference {
        return db.collection("travels")
    }
}

//
//  Firebase+Storage.swift
//  pep
//
//  Created by albert vila  on 27/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation
import FirebaseStorage



enum ContentType:String {
    case imageJPG = "image/jpg"
}

protocol FSStorageProtocol {
    
    var contentType:ContentType { get }
    var storage:Storage { get }
}

extension FSStorageProtocol {
    var storage:Storage {
        return Storage.storage()
    }
    var contentType:ContentType {
        return .imageJPG
    }
}

// MARK: - Class -
class StorageProvider: FSStorageProtocol {
    
    private var metadata: StorageMetadata
    
    // MARK: - Life Cycle -
    required init(metadata: StorageMetadata = StorageMetadata()) {
        self.metadata = metadata
    }
    
    func downloadTicket(name: String = "", handler:@escaping ResultResponse<String>) {
        let path = Buckets.child.rawValue + "/\(name)"
        let ref = storage.reference().child(path)
        
        ref.downloadURL { url, error in
            guard error == nil else {
                handler(.failure(error!)); return
            }
            guard let urlString = url?.absoluteString else {
                handler(.failure(PepError.fileNotExists)); return
            }
            handler(.success(urlString))
        }
    }
}

//extension Array where Element == String {
//    
//    func getTicketsURL(handler:@escaping ([String], Error?) -> Void) {
//        let group      = DispatchGroup()
//        let storage    = StorageProvider()
//        
//        var collection = [String]()
//        var error:PepError?
//        
//        forEach { name in
//            group.enter()
//            storage.downloadTicket(name: name) { result in
//                switch result {
//                case .success(let value):
//                    collection.append(value)
//                case .failure(_):
//                    error = PepError.fileNotExists
//                }
//                group.leave()
//            }
//        }
//        group.notify(queue: .main) {
//            handler(collection, error)
//        }
//    }
//}

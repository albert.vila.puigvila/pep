//
//  PEPBuckets.swift
//  pep
//
//  Created by albert vila  on 28/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation

class PEPBuckets {
    
    static let buildNewPath:(SeasonType) -> String = { bucket in
        return bucket.toString + "/" + UUID().uuidString + ".jpg"
    }
    
    static let buildExistingPath:(SeasonType, String) -> String = { bucket, name in
        return bucket.toString + "/" + name
    }
}

//
//  Picture.swift
//  pep
//
//  Created by albert vila  on 28/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation

struct Picture {
    var name:String
    var url:String?
    var createdAt:String
    
    init() {
        self.name = ""
        self.createdAt = String(Date().timeIntervalSince1970)
    }
}

extension Array where Element == Picture {
    
    func getURLs(season: SeasonType, handler:@escaping ([Picture]) -> Void) {
        let group      = DispatchGroup()
        let storage    = Database()
        
        var collection = [Picture]()
        
        forEach { photo in
            var newPhoto = photo
            group.enter()
            
            storage.getPhotosName(name: photo.name, season: season, handler: { result in
                switch result {
                case .success(let value):
                    newPhoto.url = value
                    collection.append(newPhoto)
                case .failure(_):
                    break
                }
                group.leave()
            })
        }
        
        group.notify(queue: .main) {
            handler(collection)
        }
    }
}

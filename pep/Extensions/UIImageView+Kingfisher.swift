//
//  UIImageView+Kingfisher.swift
//  pep
//
//  Created by albert vila  on 28/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit
import Kingfisher


public extension UIImageView {
    
    private func optionsInfo() -> KingfisherOptionsInfo {
        let processor = DownsamplingImageProcessor(size: frame.size)
        return [.processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage]
    }

    func setCachedImage<T>(value: T?, placeholder: UIImage? = nil, animated: Bool = true) {
        if animated { alpha = 0 }
        
        guard let some = value else {
            image = placeholder
            fadeIn()
            return
        }
        
        if let stringUrl = some as? String {
            let url = URL(string: stringUrl)
            self.kf.setImage(with: url, placeholder: placeholder)
        }
        if let url = some as? URL {
            self.kf.setImage(with: url, placeholder: placeholder)
        }
        
        if animated { fadeIn() }
    }
}

//
//  UICollectionView.swift
//  pep
//
//  Created by albert vila  on 20/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

#if os(iOS) || os(tvOS)
extension NSObject {
    public var className: String {
        return type(of: self).className
    }
    public static var className: String {
        return String(describing: self)
    }
}
#endif

extension UICollectionView {
    
    public func registerCustomCell<T: UICollectionViewCell>(type: T.Type) {
        self.register(T.self, forCellWithReuseIdentifier: T.className)
    }
    
    public func registerCustomCellNib<T: UICollectionViewCell>(type: T.Type) {
        self.register(UINib.init(nibName: T.className, bundle: nil), forCellWithReuseIdentifier: T.className)
    }
    
    /// SwiftHandle: get cell by type instead of string
    public func dequeueReusableCell<T:UICollectionViewCell>(type: T.Type, for indexPath:IndexPath) -> T? {
        return self.dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as? T
    }
}

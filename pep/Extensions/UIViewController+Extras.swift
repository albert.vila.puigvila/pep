//
//  UIViewController+Extras.swift
//  pep
//
//  Created by albert vila  on 20/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

public extension UIViewController {
    
    class func loadFromNib<T: UIViewController>() -> T {
        return T(nibName: String(describing: T.self), bundle: nil)
    }
    
    class func fromNib<T : UIViewController>() -> T {
        return T(nibName:String(describing: T.self), bundle: nil)
    }
}



public let UIViewDefaultTransitionDuration: TimeInterval = 0.45
public let UIViewDefaultFadeDuration: TimeInterval = 0.4
private let UIViewAnimationDuration: TimeInterval = 1
private let UIViewAnimationSpringDamping: CGFloat = 0.5
private let UIViewAnimationSpringVelocity: CGFloat = 0.5

extension UIView {
    
    public func animate(duration: TimeInterval, animations: @escaping (() -> Void), completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: animations, completion: completion)
    }
    
    public func animate(animations: @escaping (() -> Void), completion: ((Bool) -> Void)? = nil) {
        animate(duration: UIViewAnimationDuration, animations: animations, completion: completion)
    }
    
    public func transformScale(duration _duration:TimeInterval = UIViewDefaultTransitionDuration, x xX: CGFloat = 0, y yY: CGFloat = 0, completion: ((Bool) -> Void)? = nil) {
        self.animate(duration: _duration, animations: {
            self.transform = CGAffineTransform(scaleX: xX, y: yY)
        }, completion: completion)
    }
    
    // MARK: - animate tranlations -
    /// EZSE: back to origin with animation
    public func setTranslation(duration _duration:TimeInterval? = UIViewDefaultTransitionDuration, x xX: CGFloat?, y yY: CGFloat?, completion: ((Bool) -> Void)? = nil) {
        self.animate(duration: _duration ?? UIViewDefaultTransitionDuration, animations: {
            self.transform = .init(translationX: xX ?? 0.0, y: yY ?? 0.0)
        }, completion: completion)
    }
    
    /// EZSE: back to origin with animation
    public func backToOrigin(_ duration:TimeInterval? = UIViewDefaultTransitionDuration, completion: ((Bool) -> Void)? = nil) {
        self.animate(duration: duration ?? UIViewDefaultTransitionDuration, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: completion)
    }
    
//    public func setRotate(_ from: RotateFrom = .twoFourths, duration value:TimeInterval = 0) {
//        UIView.animate(withDuration: value) {
//            if from == .origin {
//                self.transform = CGAffineTransform.identity
//            } else {
//                self.transform = self.transform.rotated(by: RotateFrom.degrees(from))
//            }
//        }
//    }
    
    public func fadeOutIn(_ duration: TimeInterval = 0.35) {
        self.fadeOut(duration, delay: 0) { (success) in
            if success {
                self.fadeIn(duration, delay: 0, completion: nil)
            }
        }
    }
    
    ///EZSE: Fade in with duration, delay and completion block.
    public func fadeIn(_ duration: TimeInterval? = UIViewDefaultFadeDuration, delay: TimeInterval? = 0.0, completion: ((Bool) -> Void)? = nil) {
        fadeTo(1.0, duration: duration, delay: delay, completion: completion)
    }
    
    /// EZSwiftExtensions
    public func fadeOut(_ duration: TimeInterval? = UIViewDefaultFadeDuration, delay: TimeInterval? = 0.0, completion: ((Bool) -> Void)? = nil) {
        fadeTo(0.0, duration: duration, delay: delay, completion: completion)
    }
    public func fadeOutIfNeeded(_ duration: TimeInterval? = UIViewDefaultFadeDuration, delay: TimeInterval? = 0.0, completion: ((Bool) -> Void)? = nil) {
        if alpha > 0 {
            fadeTo(0.0, duration: duration, delay: delay, completion: completion)
        }
    }
    
    /// Fade to specific value     with duration, delay and completion block.
    public func fadeTo(_ value: CGFloat, duration: TimeInterval? = UIViewDefaultFadeDuration, delay: TimeInterval? = 0.0, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration ?? UIViewDefaultFadeDuration, delay: delay ?? UIViewDefaultFadeDuration, options: .curveEaseInOut, animations: {
            self.alpha = value
        }, completion: completion)
    }
    public func spring(duration: TimeInterval, animations: @escaping (() -> Void), completion: ((Bool) -> Void)? = nil) {
        UIView.animate(
            withDuration: UIViewAnimationDuration,
            delay: 0,
            usingSpringWithDamping: UIViewAnimationSpringDamping,
            initialSpringVelocity: UIViewAnimationSpringVelocity,
            options: UIView.AnimationOptions.allowAnimatedContent,
            animations: animations,
            completion: completion
        )
    }
    func pop() {
        setScale(x: 1.1, y: 1.1)
        spring(duration: 0.2, animations: { [unowned self] () -> Void in
            self.setScale(x: 1, y: 1)
        })
    }
    
    /// EZSwiftExtensions
    func popBig() {
        setScale(x: 1.25, y: 1.25)
        spring(duration: 0.2, animations: { [unowned self] () -> Void in
            self.setScale(x: 1, y: 1)
        })
    }
    func setScale(x: CGFloat, y: CGFloat) {
        var transform = CATransform3DIdentity
        transform.m34 = 1.0 / -1000.0
        transform = CATransform3DScale(transform, x, y, 1)
        self.layer.transform = transform
    }
}


// MARK: - Alert's -
extension UIViewController {
    
    func presentSuccessAlertController(message: String, handler:((Bool) -> Void)? = nil) {
        let alert = UIAlertController(title: "🤓 Alerta!", message: message, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Si", style: .destructive) { _ in
            handler?(true)
        }
        let cancelBtn = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(okBtn)
        alert.addAction(cancelBtn)
        
        present(alert, animated: true, completion: nil)
    }
    
    func presentErrorAlertController(message: String, handler:((Bool) -> Void)? = nil) {
        let alert = UIAlertController(title: "👹 Alerta!", message: message, preferredStyle: .alert)
        let cancelBtn = UIAlertAction(title: "D'acord", style: .default, handler: nil)
        alert.addAction(cancelBtn)
        
        present(alert, animated: true, completion: nil)
    }
    
}

//
//  AppCoordinator.swift
//  pep
//
//  Created by albert vila  on 20/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}

class AppCoordinator: Coordinator {
    
    let window: UIWindow
    let controller:UIViewController
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
//    private lazy var selectLoginCoordinator: SelectLoginCoordinator = {
//        return SelectLoginCoordinator(presenter: self.navigationController)
//    }()
    
    
    // MARK: - Life Cycle -
    init(window: UIWindow, controller: UIViewController) {
        
        self.window     = window
        self.controller = controller
        
        self.navigationController = UINavigationController(rootViewController: controller)
        self.navigationController.setNavigationBarHidden(true, animated: true)
        self.navigationController.viewControllers = [controller]
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
    }
    
    func start() {
        let vc:SelectViewController = .fromNib()
        navigationController.setViewControllers([vc], animated: false)
    }
}

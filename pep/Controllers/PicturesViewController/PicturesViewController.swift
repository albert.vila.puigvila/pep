//
//  PicturesViewController.swift
//  pep
//
//  Created by albert vila  on 20/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

class PicturesViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    
    private var pictures = [Picture]() {
        didSet { collectionView.reloadData() }
    }
    private var detailVC:DetailViewController = .fromNib()
    
    var seasonType:SeasonType?
    
    // MARK: - Life cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollection()
        loadPhotos()
    }
    
    private func loadPhotos() {
        guard let type = seasonType else {
            assertionFailure(); return
        }
        PEPHud.show()
        
        Database.shared.getPictures(season: type) { result in
            PEPHud.hide()

            switch result {
            case .success(let value):
                self.pictures = value
                if value.isEmpty {
                    self.presentErrorAlertController(message: "🥴 Hola Seguip, encara no hi ha cap foto, si vols en pots afegir.")
                }
            case .failure(let error):
                self.presentErrorAlertController(message: error.localizedDescription)
                if let pepError = error as? PepError, pepError == .empty {
                    self.pictures = []
                }
            }
        }
    }
    
    @IBAction func actAddPhotos(_ sender: Any) {
        CameraHandler.shared.showActionSheet(vc: self, in: btnAdd)
        CameraHandler.shared.imagePickedBlock = { image in
            PEPHud.show()
            
            Database.shared.sendToStorage(image: image, season: self.seasonType!, handler: { result in
                PEPHud.hide()
                
                switch result {
                case .success(let error):
                    guard let error = error else {
                        return
                    }
                    self.presentErrorAlertController(message: error.localizedDescription, handler: nil)
                case .failure(let error):
                    self.presentErrorAlertController(message: error.localizedDescription, handler: nil)
                }
            })
        }
    }
    
    @IBAction func actBack(_ sender: Any) {
        Database.shared.stopListener()
        navigationController?.popViewController(animated: true)
    }
    
    private func setupCollection() {
        lbTitle.text = seasonType?.toString
        
        collectionView.alpha      = 1
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.registerCustomCellNib(type: PictureCell.self)
    }
}

extension PicturesViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(type: PictureCell.self, for: indexPath) else {
            return UICollectionViewCell()
        }
        if let image = pictures.get(at: indexPath.item) {
            cell.load(picture: image)
        }
        return cell
    }
}

extension PicturesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let picture = pictures.get(at: indexPath.item) else {
            assertionFailure(); return
        }
        guard let cell = collectionView.cellForItem(at: indexPath) as? PictureCell else {
            return
        }
        guard let image = cell.imView.image else {
            return
        }
        detailVC.image      = image
        detailVC.picture    = picture
        detailVC.seasonType = seasonType
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension PicturesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (collectionView.bounds.width / 2) - 5
        let height = (collectionView.bounds.height / 3) - 10
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}


extension Array {
    public func get(at index: Int) -> Element? {
        guard index >= 0 && index < count else { return nil }
        return self[index]
    }
}

extension UIImage {
    
    func renderResizedImage(newWidth: CGFloat) -> UIImage {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        let renderer = UIGraphicsImageRenderer(size: newSize)
        
        let image = renderer.image { (context) in
            self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        }
        return image
    }
}

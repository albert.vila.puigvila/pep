//
//  PictureCell.swift
//  pep
//
//  Created by albert vila  on 20/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit
import Kingfisher

class PictureCell: UICollectionViewCell {

    @IBOutlet weak var imView: UIImageView!
    @IBOutlet weak var lbNumber: UILabel!
//    @IBOutlet private weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Life Cycle -
    override func prepareForReuse() {
        super.prepareForReuse()
        
//        imView.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        imView.layer.cornerRadius = 7.5
    }
    
    func loadNumber(number: Int) {
        lbNumber.text = String(number)
    }
    
    func load(picture: Picture) {
        guard let url = picture.url else {
            assertionFailure(); return
        }
        imView.kf.indicatorType = .activity
        imView.setCachedImage(value: url)
    }
}

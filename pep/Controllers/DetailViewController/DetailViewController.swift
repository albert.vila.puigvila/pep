//
//  DetailViewController.swift
//  pep
//
//  Created by albert vila  on 22/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//


import UIKit


public typealias Action = () -> Void

class DetailViewController: UIViewController, EFImageViewZoomDelegate {
    
    @IBOutlet weak var imViewZoom: EFImageViewZoom!
    @IBOutlet private weak var btnDelete: UIButton!
    
    private var hasShowDeleteButton:Bool {
        return true
    }
    
    private var scrollContentSize:CGSize = .zero
    var picture:Picture?
    var image:UIImage?
    
//    var imageName:Any?
    var seasonType:SeasonType!
    var deleteItem:Action = {  }
    
    // MARK: - Life cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        guard seasonType != nil else {
            assert(seasonType != nil)
            presentErrorAlertController(message: "Alguna cosa no ha anat bé.") { success in
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            return
        }
        guard picture != nil else {
            assert(picture != nil)
            presentErrorAlertController(message: "Alguna cosa no ha anat bé.") { success in
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.imViewZoom._delegate = self
        self.imViewZoom.contentMode = .center
        
        imViewZoom.image = image
    }
    
    @IBAction func actDelete(_ sender: Any) {
        guard let name = picture?.name else {
            assertionFailure(); return
        }
        PEPHud.show()
        
        Database.shared.deletePhotoFromEverywhere(name: name, season: seasonType) { error in
            PEPHud.hide()
            
            guard error == nil else {
                return
            }
            self.deleteItem()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actBack(_ sender: Any) {
        imViewZoom.fadeOut(0.15) { [weak self]_ in
            self?.navigationController?.popViewController(animated: true)
            self?.imViewZoom.doubleTapped()
            self?.imViewZoom.fadeIn(0.1, delay: 0.65, completion: nil)
        }
    }
}

//
//  SelectCell.swift
//  pep
//
//  Created by albert vila  on 23/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

class SelectCell: UICollectionViewCell {

    @IBOutlet private weak var imView: UIImageView!
    @IBOutlet private weak var lbTitle: UILabel!
    @IBOutlet private weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView?.layer.cornerRadius = 5
    }
    
    func load(item: Season) {
        imView.image = UIImage(named: item.imageName)
        lbTitle.text = item.title
    }
}

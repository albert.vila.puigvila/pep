//
//  SelectViewController.swift
//  pep
//
//  Created by albert vila  on 23/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//


import UIKit

class SelectViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let seasons = SeasonProvider.seasons()
    private let padding = CGFloat(10)
    @IBOutlet weak var containerImage: UIView!
    @IBOutlet weak var imViewPep: UIImageView!
    
    // MARK: - Life cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setupCollection()
        hidePepHUDNoPhotos()
    }
    
    private func setupCollection() {
        
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.registerCustomCellNib(type: SelectCell.self)
    }

    private func hidePepHUDNoPhotos() {
        dispatch.runThisAfterDelay(seconds: 2, after: {
            self.imViewPep.pop()
            dispatch.runThisAfterDelay(seconds: 0.75, after: {
                self.containerImage.setTranslation(duration: 0.75, x: -UIScreen.main.bounds.width - 10, y: 0, completion: { _ in
                    self.containerImage.alpha = 0
                    self.navigationController?.popViewController(animated: true)
                })
            })
        })
//        containerImage.fadeIn(0.1) { _ in
//        }
    }
}

extension SelectViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return seasons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(type: SelectCell.self, for: indexPath) else {
            return UICollectionViewCell()
        }
        if let season = seasons.get(at: indexPath.item) {
            cell.load(item: season)
        }
        return cell
    }
}


extension SelectViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let season = SeasonType(rawValue: indexPath.item) else {
            assertionFailure(); return
        }
        let vc: PicturesViewController = .fromNib()
        vc.seasonType = season
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SelectViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (collectionView.bounds.width / 2) - (padding / 2)
        let height = (collectionView.bounds.height / (CGFloat(seasons.count / 2))) - padding
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: padding / 2, left: 0, bottom: 0, right: 0)
    }
}

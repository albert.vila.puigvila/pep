//
//  SeasonProvider.swift
//  pep
//
//  Created by albert vila  on 23/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation
import FirebaseFirestore

enum SeasonType: Int {
    case child
    case young
    case university
    case old
    case travels
    case nieces
}

extension SeasonType {
    
    static func create(of season: String) -> SeasonType {
        switch season {
        case "child":
            return .child
        case "young":
            return .young
        case "university":
            return .university
        case "old":
            return .old
        case "travels":
            return .travels
        case "nieces":
            return .nieces
        default:
            assertionFailure(); return .child
        }
    }
    
    var toString:String {
        switch self {
        case .child:
            return "child"
        case .young:
            return "young"
        case .university:
            return "university"
        case .old:
            return "old"
        case .travels:
            return "travels"
        case .nieces:
            return "nieces"
        }
    }
    
    var toDirectory:Directories {
        switch self {
        case .child:
            return .child
        case .young:
            return .young
        case .university:
            return .university
        case .old:
            return .old
        case .travels:
            return .travels
        case .nieces:
            return .nieces
        }
    }
}


extension SeasonType: FSCollectionReference {
    
    var toReference: CollectionReference {
        
        switch self {
        case .child:
            return child
        case .young:
            return young
        case .university:
            return university
        case .old:
            return old
        case .travels:
            return travels
        case .nieces:
            return nieces
        }
    }
}



struct Season {
    
    let title:String
    let imageName:String
}

class SeasonProvider {
    
    static func seasons() -> [Season] {
        let child = Season(title: "child", imageName: "ic_baby")
        let young = Season(title: "youth", imageName: "ic_teen")
        let university = Season(title: "university", imageName: "ic_university")
        let old = Season(title: "old", imageName: "ic_old")
        let travels = Season(title: "travels", imageName: "ic_travels")
        let nephews = Season(title: "nieces", imageName: "ic_nephew")
        return [child, young, university, old, travels, nephews]
    }
}

//
//  PEPHud.swift
//  pep
//
//  Created by albert vila  on 28/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit
import SVProgressHUD


// MARK: - Life cycle -
class PEPHud: UIViewController {
    
    static let shared = PEPHud()
    
    fileprivate lazy var backgroundView: UIView = {
        let element = UIView()
        element.backgroundColor = .lightGray
        element.alpha = 0.7
        return element
    }()
    
    fileprivate lazy var lottieSize:CGSize = {
        let width = self.view.frame.width / 2.5
        return CGSize(width: width, height: width)
    }()
    
    fileprivate lazy var lottieFrame: CGRect = {
        return CGRect(origin: self.view.center, size: self.lottieSize)
    }()
}

extension PEPHud {
    
    static func show() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
    }
    static func hide() {
        SVProgressHUD.dismiss()
    }
}

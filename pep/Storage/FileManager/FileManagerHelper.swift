//
//  FileManagerHelper.swift
//  pep
//
//  Created by albert vila  on 23/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

enum Directories:String {
    case child, young, university, old, travels, nieces
}

enum URLDirectories:String {
    case childUrl, youngUrl, universityUrl, oldUrl, travelsUrl, niecesUrl
}

/// https://medium.com/@ankitbansal806/save-and-get-image-from-document-directory-in-swift-5c1280ec17f5
class FileManagerHelper {
    
    static let shared = FileManagerHelper()
    
    private let fileManager = FileManager.default
    private var getDirectoryPath: String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    func createAllDirectories() {
//        createStorageURLDirectories()
        createDirectories()
    }
    
    // MARK: - Private -
    private func createStorageURLDirectories() {
        let child = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.childUrl.rawValue)
        let young = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.youngUrl.rawValue)
        let university = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.universityUrl.rawValue)
        let old = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.oldUrl.rawValue)
        let travels = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.travelsUrl.rawValue)
        let nieces = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(URLDirectories.niecesUrl.rawValue)
        
        if !fileManager.fileExists(atPath: child) {
            do {
                try fileManager.createDirectory(atPath: child, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: young) {
            do {
                try fileManager.createDirectory(atPath: young, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: university) {
            do {
                try fileManager.createDirectory(atPath: university, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: old) {
            do {
                try fileManager.createDirectory(atPath: old, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: travels) {
            do {
                try fileManager.createDirectory(atPath: travels, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: nieces) {
            do {
                try fileManager.createDirectory(atPath: nieces, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
    }
    
    private func createDirectories() {
        let child = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.child.rawValue)
        let young = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.young.rawValue)
        let university = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.university.rawValue)
        let old = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.old.rawValue)
        let travels = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.travels.rawValue)
        let nieces = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(Directories.nieces.rawValue)
        
        if !fileManager.fileExists(atPath: child) {
            do {
                try fileManager.createDirectory(atPath: child, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: young) {
            do {
                try fileManager.createDirectory(atPath: young, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: university) {
            do {
                try fileManager.createDirectory(atPath: university, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: old) {
            do {
                try fileManager.createDirectory(atPath: old, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: travels) {
            do {
                try fileManager.createDirectory(atPath: travels, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
        if !fileManager.fileExists(atPath: nieces) {
            do {
                try fileManager.createDirectory(atPath: nieces, withIntermediateDirectories: true, attributes: nil)
            } catch {
                assertionFailure()
            }
        }
    }
    
    func getImageUrls(for season: SeasonType) -> [URL] {
        guard let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            assertionFailure(); return []
        }
        let child = documentsUrl.appendingPathComponent("\(season.toDirectory.rawValue)/")
        
        do {
            let directoryContents = try FileManager
                .default
                .contentsOfDirectory(at: child, includingPropertiesForKeys: nil, options: [])
            
            return directoryContents.map { $0 }.sorted(by: { $0.path < $1.path })
            
        } catch {
            assertionFailure(); return []
        }
    }
    
    @discardableResult
    func store(image: UIImage, season: SeasonType) -> URL? {
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
            .appendingPathComponent(buildFileName(seasonType: season))
        
        let imageData = image.jpegData(compressionQuality: 0.5)
        guard fileManager.createFile(atPath: path , contents: imageData, attributes: nil) else {
            return nil
        }
        return URL(fileName: path)
    }
    
    func delete(path: String, handler:(Error?) -> Void) {
        if fileManager.fileExists(atPath: path) {
            do {
                try fileManager.removeItem(atPath: path)
            } catch let error {
                assertionFailure();
                handler(error)
            }
        } else {
            handler(PepError.fileNotExists)
        }
        handler(nil)
    }
    
    // MARK: - Firestore -
    private func buildURLStorageFileName(seasonType: SeasonType) -> String {
        let date = String(Date().timeIntervalSince1970)
        let fileName = "/" + UUID().uuidString + "-\(date).jpg"
        
        return "/" + getDirectory(type: seasonType) + fileName
    }

    // MARK: - OLD -
    private func buildFileName(seasonType: SeasonType) -> String {
        let date = String(Date().timeIntervalSince1970)
        var fileName = ""
        switch seasonType {
        case .child:
            fileName = "/child_\(date).jpg"
        case .young:
            fileName = "/young_\(date).jpg"
        case .university:
            fileName = "/university_\(date).jpg"
        case .old:
            fileName = "/old_\(date).jpg"
        case .travels:
            fileName = "/travels_\(date).jpg"
        case .nieces:
            fileName = "/nieces_\(date).jpg"
        }
        return "/" + getDirectory(type: seasonType) + fileName
    }
    
    private func getDirectory(type: SeasonType) -> String {
        switch type {
        case .child:
            return Directories.child.rawValue
        case .young:
            return Directories.young.rawValue
        case .university:
            return Directories.university.rawValue
        case .old:
            return Directories.old.rawValue
        case .travels:
            return Directories.travels.rawValue
        case .nieces:
            return Directories.nieces.rawValue
        }
    }
}

extension URL {
    public init?(fileName: String, in directory: FileManager.SearchPathDirectory = .documentDirectory, domain: FileManager.SearchPathDomainMask = .userDomainMask) {
        guard let directory = FileManager.default.urls(for: directory, in: domain).first else { return nil }
        self.init(fileURLWithPath: fileName, relativeTo: directory)
    }
}

//
//  UserDefaultsManager.swift
//  pep
//
//  Created by albert vila  on 29/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import Foundation

class UserDefaultsManager {
    
    private let defaults = UserDefaults.standard
    
    func storeNowDate() {
        defaults.set(Date().timeIntervalSince1970, forKey: DefaultsKeys.timenow.rawValue)
    }
    
//    var shouldUpdate: Bool {
//        let now = Date().timeIntervalSince1970
//        guard let old = defaults.object(forKey: DefaultsKeys.timenow.rawValue) as? Double else {
//            return true
//        }
//        let result = now - old
//        return true
//    }
}


enum DefaultsKeys: String {
    
    case timenow
}

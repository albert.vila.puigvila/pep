//
//  PicturesProvider.swift
//  pep
//
//  Created by albert vila  on 22/12/2018.
//  Copyright © 2018 albert vila . All rights reserved.
//

import UIKit

// MARK: - Move to a single file -
enum Result<Value> {
    case success(Value)
    case failure(Error)
}
typealias ResultResponse<T> = (Result<T>) -> Void


// MARK: - Class -
class PicturesProvider {
    
    private let maxNumberImages = 49
    private let maxNumberOldImages = 47
    private let maxNumberTicketsImages = 3
    private let maxNumbernNiecesImages = 8
    
    var pictureNames = [Any]()
    

    // MARK: - Static -
    static func fetchImageNames(season:SeasonType, handler: ResultResponse<[Any]>) {
        let provider = PicturesProvider(season: season)
        guard !provider.pictureNames.isEmpty else {
            handler(.failure(PepError.empty)); return
        }
        handler(.success(provider.pictureNames))
    }
    
    // MARK: - Life Cycle -
    init(season: SeasonType) {
        buildPhotos(for: season)
    }
    
    private func buildPhotos(for type: SeasonType) {
        switch type {
        case .child:
            childPhotosNames()
        case .young:
            youngPhotosNames()
        case .university:
            laiaPhotosNames()
        case .old:
            oldPhotosNames()
        case .travels:
            ticketsPhotosNames()
        case .nieces:
            niecesPhotosNames()
        }
    }
    
    private func laiaPhotosNames() {
        var idx = 0
        
        repeat {
            pictureNames.append("img_\(idx).jpg")
            idx += 1
        } while idx < maxNumberImages
        
        pictureNames += photosNames(for: .university)
    }
    
    private func childPhotosNames() {
        var idx = 0
        
        repeat {
            if idx != 3 {
                pictureNames.append("png_\(idx).png")
            }
            idx += 1
        } while idx < maxNumberOldImages
        
        pictureNames += photosNames(for: .child)
    }
    
    private func ticketsPhotosNames() {
        var idx = 0
        
        repeat {
            pictureNames.append("tickets_\(idx).png")
            idx += 1
        } while idx < maxNumberTicketsImages
        
        pictureNames += photosNames(for: .travels)
    }
    
    private func niecesPhotosNames() {
        var idx = 0
        
        repeat {
            pictureNames.append("nieces_\(idx).jpg")
            idx += 1
        } while idx < maxNumbernNiecesImages
        
        pictureNames += photosNames(for: .nieces)
    }
    
    private func youngPhotosNames() {
        var idx = 0
        
        repeat {
            pictureNames.append("young_\(idx).jpg")
            idx += 1
        } while idx < maxNumbernNiecesImages
        
        pictureNames += photosNames(for: .young)
    }
    
    
    
    private func oldPhotosNames() {
        pictureNames = photosNames(for: .old)
    }
//    private func youngPhotosNames() {
//        pictureNames = photosNames(for: .young)
//    }
//    private func travelsPhotosNames() {
//        pictureNames = photosNames(for: .young)
//    }
//    private func niecesNames() {
//        pictureNames = photosNames(for: .young)
//    }

    private func photosNames(for season: SeasonType) -> [Any] {
        return FileManagerHelper.shared.getImageUrls(for: season)
    }
}

// MARK: - LocalizedError -
enum PepError: Error {
    case empty
    case fileNotExists
    case imageNoUploaded
    case imageSoBig
}

extension PepError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .empty:
            return NSLocalizedString("Hola Seguip, encara no hi ha cap foto, si vols en pots afegir.", comment: "")
        case .fileNotExists:
            return NSLocalizedString("Hola Seguip, alguna cosa no ha anat bé.", comment: "")
        case .imageNoUploaded:
            return NSLocalizedString("Hola Seguip, aquesta imatge no s'ha pujat.", comment: "")
        case .imageSoBig:
            return NSLocalizedString("Hola Seguip, aquesta imatge és massa gran, no es pot pujar", comment: "")
        }
    }
}
